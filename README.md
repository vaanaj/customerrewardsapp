

CustomerRewardsApplication:

This application is to demonstrate the 
"A retailer offers a rewards program of its customers, awarding points based on each recorded purchase”.
A customer receives 2 points for every dollar spent over $100 in each transaction, plus 1 point for every dollar spent over $50 in each transaction.
(e.g. a $120 purchase = 2x$20 + 1x$50 = 90 points).
 
Given a record of every transaction during a three month period, calculate the reward points earned for each customer per month and total.

Steps for Building a RestfulWebApplicarion for CustomerRewardsApplication:

Step1:

Now, we will build Maven application, for this application we should have to use Java8, JDK, and Spring Boot and it is to start a Web Service with embedded Tomcat and embedded H2 Database. We need to setup DB; we can do it easily with spring Data JPA. In application. Properties file to setup the connection to preferred DB.

Step2:

Now, we will create a model and are annotated by @Entity, @Id, and @Generated Value. 

Step  3:

We have to use JpaRepository extension and annotate the interface with @Repository. 

Step 4:

Now, We will implement the actual business logic process information in CustomerRewardsController.  
CustomerRewards Package has one Controller class which is RewardsController. With RewardsController, we can define all the RESTful endpoints. This class need to be use a service layer implementation, thus RewardsController has RewardsService class and we are using @Autowired annotation. So that SpringContext is going to find the appropriate implementation.
We can also see the RewardsService class wrapped inside the RewardsController class. It has also marked with the Autowired Annotation.
A Service layer, We have the class RewardService we will see that RewardService has a CustomerRepository interface and that interface also marked with the @Autowired annotation in RewardService class.

Let’s implement the findCustomerAll (), getCustomer() and getCustomerLastTrans() endpoints users and it returns desired response. 
 
if(customer == null) it will returns 200k Http Response, else it will return 404 ResponseNotFund.

if(id == null || months == null) it will return 200K Response, else it will return INTERNAL_SERVER_ERROR.
If(!(months > 0 && months <=12) it will return json Response, else it will return INTERNAL_SEVER_ERROR.

Step5:

Now, we will demonstrate how we are going to Restful application with Postman tool.
Restful Endpoints for checking Response in Postman and Browser:
1.	@GetMapping("/customers"): It will return all Customers details.
2.	@GetMapping("/customers/{id}"): It will return all Customer details based on Customer_Id.
3.	@GetMapping("/customersLastTrans/{id}/{months}"): It will return Customers detail’s based on Customer_Id with particular month.


