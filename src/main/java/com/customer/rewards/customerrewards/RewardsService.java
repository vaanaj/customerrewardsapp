package com.customer.rewards.customerrewards;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.customer.rewards.model.Customer;

@Service
public class RewardsService {
	
	@Autowired
	private CustomerRepository customerRepository;
	
	
	public List<Customer> getCustomerAll() {
		return customerRepository.findAll();
	}
	
	public Customer getCustomerById(Integer customerId) {
		return customerRepository.findById(customerId).orElse(null);
	}
	
	public Customer getCustomerLastTrans(Integer customerId,Integer months) {
		Customer customer = customerRepository.findById(customerId).orElse(null);
		
		//Date currDate = new Date();
		Calendar cal  = Calendar.getInstance();
		cal.add(Calendar.MONTH, months);
		//Date compareDate = cal.getTime();
		
		LocalDateTime today = LocalDateTime.now();
		
		Date compareDate = Date.from(today.minusMonths(months).atZone(ZoneId.systemDefault()).toInstant());
		
		customer.setTotalPurchases(customer.getTransactions()
					.stream()
					.filter(x -> x.getSaveDate().after(compareDate) )
					.map(x -> x.getTotal().doubleValue())
					.reduce(0d, (a,b) -> a + b).doubleValue());
		
		
		customer.setRewardPoints(customer.getTransactions()
				.stream()
				.filter(x -> x.getSaveDate().after(compareDate) )
				.map(x -> x.getPoints().intValue()).reduce(0, (a,b) -> a + b).longValue());
		
		customer.setTransactions(customer.getTransactions().stream()
				.filter(x -> x.getSaveDate().after(compareDate) )
				.collect(Collectors.toSet())
				);
	
		
		
		return customer;
	}
	
//	public ResponseEntity<Customer> getCustomerById(Integer customerId) {
//		Customer customer = customerRepository.findById(customerId).orElse(null);
//		return new ResponseEntity<Customer>(customer, HttpStatus.NOT_FOUND);
//	}

}
